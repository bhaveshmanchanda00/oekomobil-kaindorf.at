<?php

namespace DynamicContentForElementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Repeater;

use DynamicContentForElementor\DCE_Helper;
use DynamicContentForElementor\Includes\Skins;

use DynamicContentForElementor\Controls\DCE_Group_Control_Transform_Element;
use DynamicContentForElementor\Controls\DCE_Group_Control_Filters_CSS;
use DynamicContentForElementor\Controls\DCE_Group_Control_Ajax_Page;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class DCE_Widget_DynamicPosts_v2 extends DCE_Widget_DynamicPosts_Base {

    public function get_name() {
        return 'dce-dynamicposts-v2';
    }

    static public function get_position() {
        return 1;
    }

    static public function is_enabled() {
        return true;
    }

    protected $depended_scripts = [
        'imagesloaded',
        'jquery-masonry',
        'jquery-swiper',
        'swiper',
        'dce-dynamicPosts-base',
        'dce-dynamicPosts-grid',
        'dce-dynamicPosts-carousel',
        'dce-dynamicPosts-timeline',
        'dce-dynamicPosts-dualcarousel',
        'dce-dynamicPosts-smoothscroll',
        'dce-dynamicPosts-gridtofullscreen3d',
        'dce-threejs-lib',
        'dce-gsap-lib',
        'dce-CSSPlugin-lib',
        'dce-ScrollToPlugin-lib',
        'dce-ScrollTrigger-lib',
        'dce-threejs-gridtofullscreeneffect',
        'dce-splitText-lib',
        'dce-dynamicPosts-crossroadsslideshow',
        'dce-dynamicPosts-nextpost',
        'isotope',
        'dce-threejs-OrbitControls',
        'dce-threejs-CSS3DRenderer',
        'dce-dynamicPosts-3d',
        'infinitescroll',
        'dce-ajaxmodal',
        // 'dce-acf_posts'
    ];

    protected $depended_styles = [
        'font-awesome',
        'elementor-icons-fa-solid',
        'animatecss',
        'dce-swiper',
        'dce-dynamicPosts',
        'dce-dynamicPosts-grid',
        'dce-dynamicPosts-carousel',
        'dce-dynamicPosts-timeline',
        'dce-dynamicPosts-dualcarousel',
        'dce-dynamicPosts-smoothscroll',
        'dce-dynamicPosts-gridtofullscreen3d',
        'dce-dynamicPosts-crossroadsslideshow',
        'dce-dynamicPosts-nextpost',
        'dce-dynamicPosts-3d'
    ];

    public function add_script_depends($handler) {
        if( !empty($handler) && is_array($handler) ){
            $this->depended_scripts[] = array_merge(
                $this->depended_scripts,
                $handler
            );
        }
        else if( !empty($handler) && is_string($handler) ){
            $this->depended_scripts[] = $handler;
        }
    }

    public function add_style_depends($handler) {
        if( !empty($handler) && is_array($handler) ){
            $this->depended_styles[] = array_merge(
                $this->depended_styles,
                $handler
            );
        }
        else if( !empty($handler) && is_string($handler) ){
            $this->depended_styles[] = $handler;
        }
    }

    public function get_script_depends() {
        return $this->depended_scripts;
    }

    public function get_style_depends() {
        return $this->depended_styles;
    }

    protected function _enqueue_scripts() {
        // if( $this->get_id() == $this->get_settings( '_skin' ) ){
            $scripts = $this->get_script_depends();
            if (isset($scripts) && !empty($scripts)) {
                foreach ($scripts as $script) {
                    wp_enqueue_script($script);
                }
            }
        // }
    }

    protected function _enqueue_styles() {
        // if( $this->get_id() == $this->get_settings( '_skin' ) ){
            $styles = $this->get_style_depends();
            if (isset($styles) && !empty($styles)) {
                foreach ($styles as $style) {
                    wp_enqueue_style($style);
                }
            }
        // }
    }

    public function get_title() {
        return __('Dynamic Posts v2', 'dynamic-content-for-elementor');
    }

    public function get_description() {
        return __('Dynamic Posts allows to build archives from lists of articles with four different queries. You can display the list with various layouts and use them to shape blocks', 'dynamic-content-for-elementor');
    }

    public function get_docs() {
        return 'https://www.dynamic.ooo/widget/dynamic-posts-v2';
    }

    public function get_icon() {
        return 'icon-dynamic_posts';
    }

    public function get_keywords() {
        return [ 'dynamic', 'ooo', 'posts', 'timeline', 'grid', 'slide', 'carousel', '3d', 'skin', 'custom', 'custom post type', 'cpt', 'item', 'loop' ];
    }


    protected function _register_skins() {
        $this->add_skin( new Skins\Skin_Grid( $this ) );
        $this->add_skin( new Skins\Skin_Carousel( $this ) );
        $this->add_skin( new Skins\Skin_DualCarousel( $this ) );
        // $this->add_skin( new Skins\Skin_GridFilters( $this ) );
        $this->add_skin( new Skins\Skin_Timeline( $this ) );
        $this->add_skin( new Skins\Skin_3D( $this ) );
        // $this->add_skin( new Skins\Skin_NextPost( $this ) );
        $this->add_skin( new Skins\Skin_Smoothscroll( $this ) );
        $this->add_skin( new Skins\Skin_Gridtofullscreen3d( $this ) );
        $this->add_skin( new Skins\Skin_CrossroadsSlideshow( $this ) );
    }

    protected function _register_controls() {
        parent::_register_controls();

        $taxonomies = DCE_Helper::get_taxonomies();
        $types = DCE_Helper::get_post_types();

        $this->start_controls_section(
            'section_query', [
                'label' => '<i class="dynicon eicon-settings" aria-hidden="true"></i> '.__('Query', 'dynamic-content-for-elementor'),
            ]
        );
        $this->add_control(
          'query_type', [
            'label' => __('Query Type', 'dynamic-content-for-elementor'),
            'type' => 'images_selector',
            'toggle' => false,
            'type_selector' => 'icon',
            'columns_grid' => 5,
            'separator' => 'before',
            'options' => [
                'get_cpt' => [
                    'title' => __('From Post Type','dynamic-content-for-elementor'),
                    'return_val' => 'val',
                    'icon' => 'fa fa-files-o',
                ],
                'dynamic_mode' => [
                    'title' => __('Dynamic','dynamic-content-for-elementor'),
                    'return_val' => 'val',
                    'icon' => 'fa fa-cogs',
                ],
                'post_parent' => [
                    'title' => __('From Post Parent','dynamic-content-for-elementor'),
                    'return_val' => 'val',
                    'icon' => 'fa fa-sitemap',
                ],
                'relationship' => [
                    'title' => __('Relationship','dynamic-content-for-elementor'),
                    'return_val' => 'val',
                    'icon' => 'fa fa-american-sign-language-interpreting',
                ],
                'specific_posts' => [
                    'title' => __('From Specific Posts','dynamic-content-for-elementor'),
                    'return_val' => 'val',
                    'icon' => 'fa fa-list-ul',
                ]
            ],
            'default' => 'get_cpt',
          ]
        );
        // --------------------------------- [ Post Parent ]
        $this->add_control(
            'specific_page_parent',
            [
                'label' => __('Show children from this parent-page', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('Page Title', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'query_type' => 'posts',
                //'object_type' => 'page',
                'condition' => [
                    'query_type' => 'post_parent',
                    'parent_source' => '',
                    'child_source' => ''
                ],
            ]
        );
        $this->add_control(
            'dynamic_parent_heading', [
                'label' => __('Dynamic', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
                'condition' => [
                    'query_type' => 'post_parent',
                ],
            ]
        );
        $this->add_control(
            'parent_source', [
                'label' => __('My Siblings', 'dynamic-content-for-elementor'),
                'description' => __('I take the post parent and I get my siblings out of myself.', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Same', 'dynamic-content-for-elementor'),
                'label_off' => __('other', 'dynamic-content-for-elementor'),
                'condition' => [
                    'query_type' => 'post_parent',
                ],
            ]
        );
        $this->add_control(
            'child_source', [
                'label' => __('My Children', 'dynamic-content-for-elementor'),
                'description' => __('Compared to myself-I take my children.', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Same', 'dynamic-content-for-elementor'),
                'label_off' => __('other', 'dynamic-content-for-elementor'),

                'condition' => [
                    'query_type' => 'post_parent',
                    'parent_source' => ''
                ],
            ]
        );
        // --------------------------------- [ Specific Posts-Pages ]
        $repeater = new Repeater();

        $repeater->add_control(
            'repeater_specific_posts',
            [
                'label' => __( 'Select Post', 'dynamic-content-for-elementor' ),
                'type'      => 'ooo_query',
                'show_label' => false,
                'placeholder'   => __( 'Select post', 'dynamic-content-for-elementor' ),
                'label_block'   => true,
                'query_type'    => 'posts',
            ]
        );
        $this->add_control(
            'specific_posts', [
                'label' => __('Specific Posts', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::REPEATER,
                'prevent_empty' => false,
                'default' => [

                ],
                'separator' => 'after',
                'fields' => array_values($repeater->get_controls()),
                'title_field' => 'ID: {{{ repeater_specific_posts }}}',
                'condition' => [
                    'query_type' => 'specific_posts',
                ],
            ]
        );
        // --------------------------------- [ META relationship ]
        $this->add_control(
            'relationship_meta', [
                'label' => __('Relationship Custom Meta Field', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SELECT,
                'options' => DCE_Helper::get_acf_field_relational_post(),
                'label_block' => true,
                'default' => '0',
                'condition' => [
                    'query_type' => 'relationship',
                ],
            ]
        );
        $this->add_control(
            'relationship_invert', [
                'label' => __('Invert direction', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SWITCHER,
                'description' => __('For Bidirectional Relationships, retrive all posts that are associated to current post', 'dynamic-content-for-elementor'),
                'condition' => [
                    'query_type' => 'relationship',
                ],
            ]
        );
        // --------------------------------- [ Custom Post Type ]
        $this->add_control(
            'post_type', [
                'label' => __('Post Type', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SELECT2,
                'options' => $types,
                'multiple' => true,
                'label_block' => true,
                'default' => [],
                'condition' => [
                    'query_type' => 'get_cpt',
                ],
            ]
        );

        $this->add_control(
            'post_status', [
                'label' => __('Post Status', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SELECT2,
                'options' => [
                    'publish' => __('Publish', 'dynamic-content-for-elementor'),
                    'draft' => __('Draft', 'dynamic-content-for-elementor'),
                    'private' => __('Private', 'dynamic-content-for-elementor'),
                    'password' => __('Password', 'dynamic-content-for-elementor'),
                ],
                'multiple' => true,
                'label_block' => true,
                'default' => [],
                'condition' => [
                    'query_type' => ['get_cpt', 'dynamic_mode'],
                ],
            ]
        );

        $this->add_control(
            'ignore_sticky_posts', [
                'label' => __('Ignore Sticky Posts', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'condition' => [
                    'query_type' => ['get_cpt', 'dynamic_mode']
                ]
            ]
        );
        $this->add_control(
            'hr_query',
            [
                'type' => Controls_Manager::DIVIDER,
                'style' => 'thick',
            ]
        );
        $this->add_control(
            'num_posts', [
                'label' => __('Number of Posts', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::NUMBER,
                'default' => '0',
                'condition' => [
                    'query_type' => ['get_cpt', 'dynamic_mode', 'relationship'],
                ],
            ]
        );
        $this->add_control(
            'post_offset', [
                'label' => __('Posts Offset', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::NUMBER,
                'default' => '0',
                'condition' => [
                    'query_type' => ['get_cpt', 'dynamic_mode'],
                    'num_posts!' => '-1'
                ],
            ]
        );
        $this->add_control(
            'orderby', [
                'label' => __('Order By', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SELECT,
                'options' => DCE_Helper::get_post_orderby_options(),
                'default' => 'date',
            ]
        );
        $this->add_control(
            'metakey', [
                'label' => __('Meta Field', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('Meta key', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'query_type' => 'metas',
                'object_type' => 'post',
                'separator' => 'after',
                'condition' => [
                    'orderby' => ['meta_value_date', 'meta_value_num'],
                ]
            ]
        );
        $this->add_control(
            'order', [
                'label' => __('Order', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'Ascending',
                    'DESC' => 'Descending'
                ],
                'default' => 'DESC',
                'condition' => [
                    /* 'query_type' => ['get_cpt', 'dynamic_mode', 'acf_relations'], */
                    'orderby!' => ['random']
                ],
            ]
        );
        // --------------------------------- [ Posts Exclusion ]
        $this->add_control(
            'heading_query_options', [
                'label' => __('Exclude', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
                'condition' => [
                    'query_type' => ['get_cpt', 'dynamic_mode']
                ]
            ]
        );
        $this->add_control(
            'exclude_io', [
                'label' => __('Current Post', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'condition' => [
                    'query_type' => ['get_cpt', 'dynamic_mode']
                ]
            ]
        );
        $this->add_control(
            'exclude_page_parent', [
                'label' => __('Page parent', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SWITCHER,
                'condition' => [
                    'query_type' => ['get_cpt', 'dynamic_mode']
                ]
            ]
        );
        $this->add_control(
            'exclude_posts', [
                'label' => __('Specific Posts', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('Post Title', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'query_type' => 'posts',
                'multiple' => true,
                'condition' => [
                    'query_type' => ['get_cpt', 'dynamic_mode'],
                ],
            ]
        );
        $this->end_controls_section();




        // ------------------------------------------------------------------ [SECTION QUERY-FILTER]
        $this->start_controls_section(
            'section_query_filter', [
                'label' => '<i class="dynicon eicon-parallax" aria-hidden="true"></i> '.__('Query Filter', 'dynamic-content-for-elementor'),
                'condition' => [
                    'query_type' => ['get_cpt', 'dynamic_mode']
                ]
            ]
        );
        $this->add_control(
            'query_filter', [
                'label' => __('By', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SELECT2,
                'options' => [
                    'date' => 'Date',
                    'term' => 'Term',
                    'author' => 'Author',
                    'metakey' => 'Meta key',
                ],
                'multiple' => true,
                'label_block' => true,
                'default' => [],
            ]

        );

        // +********************* Date
        $this->add_control(
            'heading_query_filter_date',
            [
                'type' => Controls_Manager::RAW_HTML,
                'show_label' => false,
                'raw' => '<i class="fa fa-calendar" aria-hidden="true"></i> '.__(' Date Filters', 'dynamic-content-for-elementor'),
                'label_block' => false,
                'separator' => 'before',
                'content_classes' => 'dce-icon-heading',
                'condition' => [
                    'query_filter' => 'date',
                ],
            ]
        );
        $this->add_control(
            'querydate_mode', [
                'label' => __('Date Filter', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SELECT,
                'default' => '',
                'label_block' => true,
                'options' => [
                    '' => __('No Filter', 'dynamic-content-for-elementor'),
                    'past' => __('Past', 'dynamic-content-for-elementor'),
                    'future' => __('Future', 'dynamic-content-for-elementor'),
                    'today' => __('Today', 'dynamic-content-for-elementor'),
                    'yesterday' => __('Yesterday', 'dynamic-content-for-elementor'),
                    'days' => __('Past Days', 'dynamic-content-for-elementor'),
                    'weeks' => __('Past Weeks', 'dynamic-content-for-elementor'),
                    'months' => __('Past Months', 'dynamic-content-for-elementor'),
                    'years' => __('Past Years', 'dynamic-content-for-elementor'),
                    'period' => __('Period', 'dynamic-content-for-elementor'),
                ],
                'condition' => [
                    'query_filter' => 'date',
                ],
            ]
        );
        $this->add_control(
            'querydate_field', [
                'label' => __('Date Field', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'options' => [
                    'post_date' => [
                        'title' => __('Publish Date', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-calendar',
                    ],
                    'post_modified' => [
                    'title' => __('Modified Date', 'dynamic-content-for-elementor'),
                    'icon' => 'fa fa-edit',
                    ],
                    'post_meta' => [
                        'title' => __('Post Meta', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-square',
                    ],
                ],
                'default' => 'post_date',
                'toggle' => false,
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode!' => ['', 'future'],
                ],
            ]
        );
        $this->add_control(
            'querydate_field_meta',
            [
                'label' => __('Meta Field', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('Meta key or Name', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'query_type' => 'metas',
                'object_type' => 'post',
                'description' => __('Selected Post Meta value must be stored if format "Ymd", like ACF Date', 'dynamic-content-for-elementor'),
                'separator' => 'before',
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode!' => 'future',
                    'querydate_field' => 'post_meta'
                ]
            ]
        );
        $this->add_control(
            'querydate_field_meta_format',
            [
                'label' => __('Meta Date Format', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Ymd', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'default' => __('Ymd', 'dynamic-content-for-elementor'),
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode!' => 'future',
                    'querydate_field' => 'post_meta'
                ]
            ]
        );
        $this->add_control(
            'querydate_field_meta_future',
            [
                'label' => __('Meta Field', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('Meta key or Name', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'query_type' => 'metas',
                'object_type' => 'post',
                'description' => __('Selected Post Meta value must be stored if format "Ymd", like ACF Date', 'dynamic-content-for-elementor'),
                'separator' => 'before',
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode' => 'future',
                ]
            ]
        );
        $this->add_control(
            'querydate_field_meta_future_format',
            [
                'label' => __('Meta Date Format', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Y-m-d', 'dynamic-content-for-elementor'),
                'label_block' => false,
                'default' => __('Ymd', 'dynamic-content-for-elementor'),
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode' => 'future',
                ]
            ]
        );
        // number of days / months / years elapsed
        $this->add_control(
            'querydate_range', [
                'label' => __('Number of (days/months/years) elapsed', 'dynamic-content-for-elementor'),
                'label_block' => false,
                'type' => Controls_Manager::NUMBER,
                'default' => 1,
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode' => ['days', 'weeks', 'months', 'years']
                ]
            ]
        );


        $this->add_control(
            'querydate_date_type', [
                'label' => __('Date Input Mode', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => true,
                'options' => [
                    'static' => [
                        'title' => __('Static', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-calendar-plus-o',
                    ],
                    'dynamicstring' => [
                        'title' => __('Dynamic', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-cogs',
                    ],
                ],
                'default' => '_dynamic',
                'toggle' => false,
                'separator' => 'before',
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode' => 'period'
                ],
            ]
        );
        $this->add_control(
            'querydate_date_from', [
                'label' => __('Date FROM', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::DATE_TIME,
                'label_block' => false,
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode' => 'period',
                    'querydate_date_type' => 'static',
                ],
            ]
        );
        $this->add_control(
            'querydate_date_to', [
                'label' => __('Date TO', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::DATE_TIME,
                'label_block' => false,
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode' => 'period',
                    'querydate_date_type' => 'static',
                ],
            ]
        );
        $this->add_control(
            'querydate_date_from_dynamic', [
                'label' => __('Date FROM', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::TEXT,
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode' => 'period',
                    'querydate_date_type' => 'dynamicstring',
                ],
            ]
        );
        $this->add_control(
            'querydate_date_to_dynamic', [
                'label' => __('Date TO', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::TEXT,
                'condition' => [
                    'query_filter' => 'date',
                    'querydate_mode' => 'period',
                    'querydate_date_type' => 'dynamicstring',
                ],
            ]
        );
        // +********************* Term Taxonomy
        $this->add_control(
            'heading_query_filter_term',
            [
                'type' => Controls_Manager::RAW_HTML,
                'show_label' => false,
                'raw' => '<i class="fa fa-folder-o" aria-hidden="true"></i> '.__(' Term Filters', 'dynamic-content-for-elementor'),
                'separator' => 'before',
                'content_classes' => 'dce-icon-heading',
                'condition' => [
                    'query_filter' => 'term'
                ],
            ]
        );
        // From Post or Meta
        $this->add_control(
            'term_from', [
                'label' => __('Type', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'options' => [
                    'post_term' => [
                        'title' => __('Select Term', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-tag',
                    ],
                    'post_meta' => [
                        'title' => __('Post Meta Term', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-square',
                    ],
                    'dynamicstring' => [
                        'title' => __('Dynamic String', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-cogs',
                    ],
                ],
                'default' => 'post_term',
                'toggle' => false,
                'condition' => [
                    'query_filter' => 'term'
                ],
            ]
        );
        $this->add_control(
            'taxonomy', [
                'label' => __('Select Taxonomy', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SELECT,
                //'options' => get_post_taxonomies( $post->ID ),
                'options' => ['' => __('All', 'dynamic-content-for-elementor')] + $taxonomies, //get_taxonomies(array('public' => true)),
                'default' => '',
                'description' => __('Filter results by selected taxonomy', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'condition' => [
                    // 'term_from' => 'post_term',
                    // 'term_from' => 'post_meta',
                    // 'term_from' => 'dynamicstring',
                    'query_filter' => 'term'
                ],
            ]
        );
        // [Post Meta]
        $this->add_control(
            'term_field_meta',
            [
                'label' => __('Post Term <b>custom meta field</b>', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('Meta key or Name', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'query_type' => 'metas',
                'object_type' => 'post',
                'description' => __('Selected Post Meta value. Il meta deve restituire un\'elemento di tipo array o stringa separata da virgola che contiene gli ID di tipo termine. (es: array[5,27,88] o 5,27,88)', 'dynamic-content-for-elementor'),
                'condition' => [
                    'term_from' => 'post_meta',
                    'query_filter' => 'term'
                ]
            ]
        );
        // [Post Meta String]
        $this->add_control(
            'term_field_meta_string', [
                'label' => __('Post Term <b>string field</b>', 'dynamic-content-for-elementor'),
                'description' => __('Write Post Meta value. Scrivi una sequenza di ID di tipo termine separati da virgola. (es: "5,27,88")', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'render_type' => 'template',
                'default' => '',
                'condition' => [
                    'term_from' => 'dynamicstring',
                    'query_filter' => 'term'
                ]
            ]
        );
        // [Post Term]
        foreach ($taxonomies as $tax_key => $a_tax) {
            if ($tax_key) {
                $this->add_control(
                    'include_term_' . $tax_key,
                    [
                        'label' => __('<b>Include</b> Terms of ' . $a_tax, 'dynamic-content-for-elementor'),
                        'type' => 'ooo_query',
                        'placeholder' => __('All terms', 'dynamic-content-for-elementor'),
                        'label_block' => true,
                        'query_type' => 'terms',
                        'object_type' => $tax_key,
                        'description' => __('Filter results by selected taxonomy term', 'dynamic-content-for-elementor'),
                        'render_type' => 'template',
                        'multiple' => true,
                        'condition' => [
                            'taxonomy' => $tax_key,
                            'query_filter' => 'term',
                            'term_from' => 'post_term',
                            'terms_current_post' => ''
                        ],
                    ]
                );
            }
        }

        $this->add_control(
            'include_term_operator',
            [
                'label' => __('<b>Include</b> Operator', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'AND' => [
                        'title' => __('AND', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-circle',
                    ],
                    'IN' => [
                        'title' => __('IN', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-circle-o',
                    ]
                ],
                'toggle' => false,

                'default' => 'IN',

                'conditions' => [
                    'terms' => [
                        [
                            'name' => 'taxonomy',
                            'operator' => '!=',
                            'value' => '',
                        ],
                        [
                            'name' => 'term_from',
                            'operator' => '==',
                            'value' => 'post_term',
                        ],
                    ]
                ]
            ]
        );
        foreach ($taxonomies as $tax_key => $a_tax) {
                if ($tax_key) {
                $this->add_control(
                    'exclude_term_' . $tax_key,
                    [
                        'label' => __('<b>Exclude</b> Term for ' . $a_tax, 'dynamic-content-for-elementor'),
                        'type' => 'ooo_query',
                        'placeholder' => __('All terms', 'dynamic-content-for-elementor'),
                        'label_block' => true,
                        'query_type' => 'terms',
                        'object_type' => $tax_key,
                        'description' => __('Filter results by selected taxonomy term', 'dynamic-content-for-elementor'),
                        'render_type' => 'template',
                        'multiple' => true,
                        'condition' => [
                            'taxonomy' => $tax_key,
                            'query_filter' => 'term',
                            'term_from' => 'post_term',
                            'terms_current_post' => ''
                        ],
                    ]
                );
            }
        }
        // $this->add_control(
        //     'terms_from_acf', [
        //         'label' => __('Use ACF Taxonomy', 'dynamic-content-for-elementor'),
        //         'type' => Controls_Manager::SWITCHER,
        //         'description' => __('The results of the filter for taxonomy terms indicated in the ACF Taxonomy type field. If the ACF Taxonomy value in the post is set, it will ignore the previously defined include / exclude filters.', 'dynamic-content-for-elementor'),
        //         'separator' => 'before',
        //         'condition' => [
        //             'taxonomy!' => '',
        //             'query_filter' => 'term',
        //             'term_from' => 'post_term'
        //         ],
        //     ]
        // );
        $this->add_control(
            'terms_current_post', [
                'label' => __('Dynamic Current Post Terms', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SWITCHER,
                'description' => __('Filter results by taxonomy terms associated to current post', 'dynamic-content-for-elementor'),
                'separator' => 'before',
                'condition' => [
                    'taxonomy!' => '',
                    'query_filter' => 'term',
                    'term_from' => 'post_term',
                ],
            ]
        );

        // +********************* Author
        $this->add_control(
            'heading_query_filter_author',
            [
                'type' => Controls_Manager::RAW_HTML,
                'show_label' => false,
                'raw' => '<i class="fa fa-user-circle-o" aria-hidden="true"></i> '.__(' Author Filters', 'dynamic-content-for-elementor'),
                'separator' => 'before',
                'content_classes' => 'dce-icon-heading',
                'condition' => [
                    'query_filter' => 'author'
                ],
            ]
        );
        // From, Post, Meta or Current
        $this->add_control(
            'author_from', [
                'label' => __('Type', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'options' => [
                    'post_author' => [
                        'title' => __('Select Author', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-users',
                    ],
                    'post_meta' => [
                        'title' => __('Post Meta Author', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-square',
                    ],
                    'dynamicstring' => [
                        'title' => __('Dynamic String', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-cogs',
                    ],
                    'current_author' => [
                        'title' => __('Current author', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-user-circle-o',
                    ],
                ],
                'default' => 'post_author',
                'toggle' => false,
                'condition' => [
                    'query_filter' => 'author'
                ],
            ]
        );
        // [Post Meta]
        $this->add_control(
            'author_field_meta',
            [
                'label' => __('Post author <b>custom meta field</b>', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('Meta key or Name', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'query_type' => 'metas',
                'object_type' => 'post',
                'default' => 'nickname',
                'description' => __('Selected Post Meta value. The meta must return an element of type array or comma separated string containing author IDs. (eg: array [5,27,88] or 5,27,88)', 'dynamic-content-for-elementor'),
                'condition' => [
                    'author_from' => 'post_meta',
                    'query_filter' => 'author'
                ]
            ]
        );
        // [Post Meta String]
        $this->add_control(
            'author_field_meta_string', [
                'label' => __('Post Author <b>string field</b>', 'dynamic-content-for-elementor'),
                'description' => __('Write Post Meta value. Write a sequence of author IDs separated by commas. (eg: 5,27,88)', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'render_type' => 'template',
                'default' => '',
                'condition' => [
                    'author_from' => 'dynamicstring',
                    'query_filter' => 'author'
                ]
            ]
        );
        // [Select Authors]
        $this->add_control(
            'include_author',
            [
                'label' => __('<b>Include</b> Author', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('Select author', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'multiple' => true,
                'query_type' => 'users',
                //'object_type'   => 'editor',
                'description' => __('Filter posts by selected Authors', 'dynamic-content-for-elementor'),
                'condition' => [
                    'query_filter' => 'author',
                    'author_from' => 'post_author'
                ]
            ]
        );

        $this->add_control(
            'exclude_author',
            [
                'label' => __('<b>Exclude</b> Author', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('No', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'multiple' => true,
                'query_type' => 'users',
                //'object_type'   => 'editor',
                'description' => __('Filter posts by selected Authors', 'dynamic-content-for-elementor'),
                'separator' => 'after',
                'condition' => [
                    'query_filter' => 'author',
                    'author_from' => 'post_author'
                ]
            ]
        );
        // ****************** Meta key
        $this->add_control(
            'heading_query_filter_metakey',
            [
                'type' => Controls_Manager::RAW_HTML,
                'show_label' => false,
                'raw' => '<i class="fa fa-key" aria-hidden="true"></i> '.__(' Metakey Filters', 'dynamic-content-for-elementor'),
                'separator' => 'before',
                'content_classes' => 'dce-icon-heading',
                'condition' => [
                    'query_filter' => 'metakey'
                ],
            ]
        );
        // From Post or Meta
        $this->add_control(
            'metakey_from', [
                'label' => __('Type', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'options' => [
                    'post_metakey' => [
                        'title' => __('Select Metakey', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-key',
                    ],
                    'post_meta' => [
                        'title' => __('Post Meta Key', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-square',
                    ],
                    'dynamicstring' => [
                        'title' => __('Dynamic String', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-cogs',
                    ],
                ],
                'default' => 'post_metakey',
                'toggle' => false,
                'condition' => [
                    'query_filter' => 'metakey'
                ],
            ]
        );
        // [Post Meta]
        $this->add_control(
            'metakey_field_meta',
            [
                'label' => __('Post Metakey <b>custom meta field</b>', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('Meta key or Name', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'query_type' => 'metas',
                'object_type' => 'post',
                'description' => __('Selected Post Meta value. The meta must return an element of type array or comma separated string that contains IDs of type metakey. (ex: array [5,27,88] or 5,27,88)', 'dynamic-content-for-elementor'),
                'condition' => [
                    'metakey_from' => 'post_meta',
                    'query_filter' => 'metakey'
                ]
            ]
        );
        // [Post Meta String]
        $this->add_control(
            'metakey_field_meta_string', [
                'label' => __('Post Metakey <b>string field</b>', 'dynamic-content-for-elementor'),
                'description' => __('Write Post Meta value. Write a sequence of metakey-type IDs separated by commas. (es: "5,27,88")', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'render_type' => 'template',
                'default' => '',
                'condition' => [
                    'metakey_from' => 'dynamicstring',
                    'query_filter' => 'metakey'
                ]
            ]
        );
        // [Post Term]
        $this->add_control(
            'include_metakey',
            [
                'label' => __('<b>Include</b> Metakey', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('All metakeys', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'query_type' => 'metakeys',
                //'object_type' => $tkey,
                'description' => __('Filter results by selected taxonomy metakey', 'dynamic-content-for-elementor'),
                'render_type' => 'template',
                'multiple' => true,
                'condition' => [
                    'query_filter' => 'metakey',
                    'metakey_from' => 'post_metakey'
                ],
            ]
        );
        $this->add_control(
            'include_metakey_combination',
            [
                'label' => __('<b>Include</b> Combination', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'AND' => [
                        'title' => __('AND', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-circle',
                    ],
                    'OR' => [
                        'icon' => 'fa fa-circle-o',
                    ]
                ],
                'toggle' => false,

                'default' => 'OR',

                'conditions' => [
                    'terms' => [
                        [
                            'name' => 'query_filter',
                            'operator' => 'contains',
                            'value' => 'metakey',
                        ],
                        [
                            'name' => 'query_filter',
                            'operator' => '!=',
                            'value' => [],
                        ],
                        [
                            'name' => 'include_metakey',
                            'operator' => '!=',
                            'value' => '',
                        ],
                        [
                            'name' => 'include_metakey',
                            'operator' => '!=',
                            'value' => [],
                        ],
                        [
                            'name' => 'metakey_from',
                            'value' => 'post_metakey',
                        ],
                    ]
                ]
            ]
        );
        $this->add_control(
            'exclude_metakey',
            [
                'label' => __('<b>Exclude</b> Metakey', 'dynamic-content-for-elementor'),
                'type' => 'ooo_query',
                'placeholder' => __('All metakeys', 'dynamic-content-for-elementor'),
                'label_block' => true,
                'query_type' => 'metakeys',
                //'object_type' => $tkey,
                'description' => __('Filter results by selected taxonomy metakey', 'dynamic-content-for-elementor'),
                'render_type' => 'template',
                'multiple' => true,
                'condition' => [
                    'query_filter' => 'metakey',
                    'metakey_from' => 'post_metakey'
                ],
            ]
        );
        $this->add_control(
            'exclude_metakey_combination',
            [
                'label' => __('<b>Exclude</b> Combination', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'AND' => [
                        'title' => __('AND', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-circle',
                    ],
                    'OR' => [
                        'title' => __('OR', 'dynamic-content-for-elementor'),
                        'icon' => 'fa fa-circle-o',
                    ]
                ],
                'toggle' => false,

                'default' => 'OR',
                'conditions' => [
                    'terms' => [
                        [
                            'name' => 'query_filter',
                            'operator' => 'contains',
                            'value' => 'metakey',
                        ],
                        [
                            'name' => 'query_filter',
                            'operator' => '!=',
                            'value' => [],
                        ],
                        [
                            'name' => 'exclude_metakey',
                            'operator' => '!=',
                            'value' => '',
                        ],
                        [
                            'name' => 'exclude_metakey',
                            'operator' => '!=',
                            'value' => [],
                        ],
                        [
                            'name' => 'metakey_from',
                            'value' => 'post_metakey',
                        ],
                    ]
                ]
            ]
        );
        $this->end_controls_section();
    }

    public function render() {

        $is_imagemask = $this->get_settings( 'imagemask_popover' );
        if($is_imagemask){
            $mask_shape_type = $this->get_settings( 'mask_shape_type' );
            $this->render_svg_mask($mask_shape_type);
        }
    }

    public function query_posts() {
        $settings = $this->get_settings_for_display();
        if ( empty($settings) )
            return;

        $post_type = $settings['post_type'];

        $id_page = DCE_Helper::get_the_id();
        $type_page = get_post_type();

        /*
         * Set query args
         */
        $args = array();
        $taxquery = array();
        $exclude_io = array();
        $posts_excluded = array();
        $use_parent_page = array();
        $terms_query = 'all';
        $terms_query_exclued = array();
        // $meta_query = array();
        $post_status = '';

        if ( empty($settings['post_status']) )
            $post_status = 'publish';
        else $post_status = $settings['post_status'];

        if ( is_singular() ) {
            if ($settings['exclude_io'])
                $exclude_io = array($id_page);
        }
        else if ( is_home() || is_archive() ) {
            $exclude_io = array();
        }

        if ($settings['exclude_posts'])
            $posts_excluded = $settings['exclude_posts'];

        if ($settings['exclude_page_parent']) {
            $use_parent_page = array(0);
        }
        else {
            $use_parent_page = array();
        }

        if($settings['ignore_sticky_posts']){
            $args['ignore_sticky_posts'] = '1';
        }
        else{
            $args['ignore_sticky_posts'] = '0';
        }

        // Tipo di query - Dynamic
        if ($settings['query_type'] == 'dynamic_mode') {

            $array_taxquery = [];
            $taxonomy_list = [];

            if (is_archive()) {
                // Considero se sono in un archivio (Term)
                $queried_object = get_queried_object();
                if (is_tax() || is_category() || is_tag()) {
                    $taxonomy_list[0] = $queried_object->taxonomy;
                }
                if (is_date()) {

                }
            } else if (is_home()) {
                // Considero se sono in home del type
            } else if (is_single()) {
                // Considero se sono in un single-post (Correlati)
                $taxonomy_list = get_post_taxonomies($id_page);
            }


            if (!empty($taxonomy_list)) {
                foreach ($taxonomy_list as $tax) {

                    $terms_list = [];
                    $lista_dei_termini = [];

                    if (is_single()) {
                        // Considero se sono in un single-post (Correlati)
                        if ($settings['taxonomy'] == $tax)
                            $terms_list = wp_get_post_terms($id_page, $tax, array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all', 'hide_empty' => true));

                        foreach ($terms_list as $term) {
                            $lista_dei_termini[] = $term->term_id;
                        }
                    } else if (is_archive()) {
                        // Considero se sono in un archivio (Term)
                        $lista_dei_termini[0] = $queried_object->term_id;
                    }
                    //echo $id_page.' '.$type_page.' '.$settings['taxonomy'];
                    //echo $tax.' - '.count($terms_list)."<br />";
                    //echo $tax . ': ' . count($lista_dei_termini);
                    if (count($lista_dei_termini) > 0) {

                        $array_taxquery = array();

                        if (count($lista_dei_termini) > 1)
                            $array_taxquery['relation'] = $settings['combination_taxonomy'];
                        //
                        foreach ($lista_dei_termini as $termine) {

                            $array_taxquery[] = array(
                                'taxonomy' => $tax,
                                'field' => 'id',
                                'terms' => $termine,
                            );
                        }
                    }


                    /* EXCLUDED */
                    $terms_query_exclued = $settings['terms_' . $tax . '_excluse'];
                    //$terms_query_exclued = $this->get_terms_query($settings, $id_page);
                    if (!empty($terms_query_exclued)) {
                        $array_taxquery_excluded = array();
                        if (count($terms_query_exclued) > 1) {
                            $array_taxquery_excluded['relation'] = $settings['combination_taxonomy_excluse'];
                        }
                        foreach ($terms_query_exclued as $term_query) {
                            $array_taxquery_excluded[] = array(
                                'taxonomy' => $tax,
                                'field' => 'term_id',
                                'terms' => $term_query,
                                'operator' => 'NOT IN',
                            );
                        }

                        if (empty($array_taxquery)) {
                            $array_taxquery = $array_taxquery_excluded;
                        } else {
                            $array_taxquery = array(
                                'relation' => 'AND',
                                $array_taxquery,
                                $array_taxquery_excluded,
                            );
                        }
                    }

                    // or and....
                    /* if (count($lista_dei_termini) > 0) {
                      $array_taxquery[] = array(
                      'taxonomy' => $tax,
                      'field' => 'id',
                      'terms' => $lista_dei_termini
                      );
                      } */
                }
            }

            // Se la taxQuery dinamica non da risultati uso quella statica.
            if (!$array_taxquery)
                $array_taxquery = $taxquery;

            if (is_array($type_page)) {
                if ($cptkey = array_search('elementor_library', $type_page))
                    $type_page[$cptkey] = 'post';
            } else {
                if ('elementor_library' == $type_page)
                    $type_page = 'post';
            }

            $args = array_merge($args, array(
                'post_type' => $type_page,
                'posts_per_page' => $settings['num_posts'],
                'order' => $settings['order'],
                'orderby' => $settings['orderby'],
                'meta_key' => $settings['metakey'],
                'post__not_in' => array_merge($posts_excluded, $exclude_io),
                'post_parent__not_in' => $use_parent_page,
                'tax_query' => $array_taxquery,
                /* 'tax_query'         => array(
                  array(
                  'taxonomy' => $settings['taxonomy'],
                  'field' => 'id',
                  'terms' => $lista_dei_termini
                  )
                  ), */
                'post_status' => $post_status,
            ));

            if (is_date()) {
                global $wp_query;
                $args['year'] = $wp_query->query_vars['year'];
                $args['monthnum'] = $wp_query->query_vars['monthnum'];
                $args['day'] = $wp_query->query_vars['day'];
            }

            if ($settings['post_offset']) {
                $args['offset'] = $settings['post_offset'];
            }

        }
        // Tipo di query - From Post Type
        else if ($settings['query_type'] == 'get_cpt') {

            $args = array_merge($args, array(
                'post_type' => $settings['post_type'],
                'posts_per_page' => $settings['num_posts'],
                'order' => $settings['order'],
                'orderby' => $settings['orderby'],
                'post_status' => $post_status,
            ));

            if ($taxquery) {
                $args['tax_query'] = $taxquery;
            }

            if ($settings['metakey']) {
                $args['meta_key'] = $settings['metakey'];
            }

            $post__not_in = array_merge($posts_excluded, $exclude_io);
            if (!empty($post__not_in)) {
                $args['post__not_in'] = $post__not_in;
            }
            if (!empty($use_parent_page)) {
                $args['post_parent__not_in'] = $use_parent_page;
            }

            if ($settings['post_offset']) {
                $args['offset'] = $settings['post_offset'];
            }
        }
        // Tipo di query - From Post Parent
        else if ($settings['query_type'] == 'post_parent') {

            if ( !empty($settings['specific_page_parent']) ) {
                $args = array_merge($args, array(
                    'post_type' => get_post_type($settings['specific_page_parent']),
                    'post_parent' => $settings['specific_page_parent'],
                ));
            }

            if( $settings['parent_source'] ){
                $args = array_merge($args, array(
                    'post_type' => $type_page,
                    'post_parent' => wp_get_post_parent_id($id_page),
                ));
            }
            if( $settings['child_source'] ){
                $args = array_merge($args, array(
                    'post_type' => $type_page,
                    'post_parent' => $id_page,
                ));
            }

            $args = array_merge($args, array(
                'order' => $settings['order'],
                'orderby' => $settings['orderby'],
            ));

        }
        // Tipo di query - Relationship
        else if ($settings['query_type'] == 'relationship') {

            if ($settings['relationship_invert']) {

                $args['meta_query'] = array(
                    array(
                        'key' => $settings['relationship_meta'],
                        'value' => '"' . $id_page . '"',
                        'compare' => 'LIKE'
                    )
                );
            }
            else{

                $relations_ids = get_post_meta($id_page, $settings['relationship_meta'], false);

                if (!empty($relations_ids) && !is_array($relations_ids)) {
                    $relations_ids = array($relations_ids);
                }
                else if (!empty($relations_ids) && is_array($relations_ids[0])) {
                    $relations_ids = $relations_ids[0];
                }

                if (!empty($relations_ids)) {
                    $args['post__in'] = $relations_ids;
                };
            }

            $args = array_merge($args, array(
                'post_type' => 'any',
                'posts_per_page' => $settings['num_posts'],
                'post_status' => 'publish',
                'orderby' => $settings['orderby'],
                'order' => $settings['order'],
            ));
        }
        // Tipo di query - From Specifc Posts
        else if ($settings['query_type'] == 'specific_posts') {
            $post__in = [];
            $specific_posts = $settings['specific_posts'];

            if( is_array($specific_posts) && !empty($specific_posts) ){
                foreach ($specific_posts as $post){
                    if( !empty($post['repeater_specific_posts']) ){
                        $post__in[] = $post['repeater_specific_posts'];
                    }
                }
            }
            else{
                $post__in = [0];
            }

            $args = array_merge($args, array(
                'post_type' => 'any',
                'post__in' => $post__in,
                'order' => $settings['order'],
                'orderby' => $settings['orderby'],
                'meta_key' => $settings['metakey'],
                'post_status' => 'publish',
            ));
        }

        global $paged;

        $paged = $this->get_current_page();
        $args['paged'] = $paged;

        /*
         * ----- Query Filter
         */
        if( is_array($settings['query_filter']) ){
            // Date query filter
            if ( in_array('date', $settings['query_filter']) ) {

                $querydate_field_meta_format = 'Ymd';

                if ( $settings['querydate_mode'] != 'future' && $settings['querydate_field'] == 'post_meta' ) {
                    $date_field_meta = sanitize_key($settings['querydate_field_meta']);
                    $querydate_field_meta_format = sanitize_text_field($settings['querydate_field_meta_format']);
                }
                if ($settings['querydate_mode'] == 'future') {
                    $date_field_meta = sanitize_key($settings['querydate_field_meta_future']);
                    $querydate_field_meta_format = sanitize_text_field($settings['querydate_field_meta_future_format']);

                    $args['meta_query'] = array(
                        array(
                            'key' => $date_field_meta,
                            'value' => date($querydate_field_meta_format, time()),
                            'meta_type' => 'DATETIME',
                            'compare' => '>='
                        )
                    );
                }
                if ($date_field) {

                    $date_after = $date_before = false;

                    switch ($settings['querydate_mode']) {
                        case 'past':
                            $date_before = date('Y-m-d H:i:s');
                            break;
                        case 'today':
                            $date_after = date('Y-m-d 00:00:00');
                            $date_before = date('Y-m-d 23:23:59');
                            break;
                        case 'yesterday':
                            $date_after = date('Y-m-d 00:00:00', strtotime('-1 day'));
                            $date_before = date('Y-m-d 23:23:59', strtotime('-1 day'));
                            break;
                        case 'days':
                        case 'weeks':
                        case 'months':
                        case 'years':
                            $date_after = '-' . $settings['querydate_range'] . ' ' . $settings['querydate_mode'];
                            $date_before = 'now';
                            break;
                        case 'period':
                            $date_after = $settings['querydate_date_from'];
                            $date_before = $settings['querydate_date_to'];
                            break;
                    }

                    // compare by post publish date
                    if ($settings['querydate_field'] == 'post_date') {
                        $args['date_query'] = array(
                            array(
                                'after' => $date_after,
                                'before' => $date_before,
                                'inclusive' => true,
                            )
                        );
                    }
                    // compare by post modified date
                    else if($settings['querydate_field'] == 'post_modified') {
                        $args['date_query'] = array(
                            array(
                                'column' => 'post_modified',
                                'after' => $date_after,
                                'before' => $date_before,
                                'inclusive' => true,
                            )
                        );
                    }
                    // compare by post meta
                    else if($settings['querydate_field'] == 'post_meta') {

                        if ($date_after){
                            $date_after = date($querydate_field_meta_format, strtotime($date_after));
                        }
                        if ($date_before){
                            $date_before = date($querydate_field_meta_format, strtotime($date_before));
                        }

                        if ($date_before && $date_after) {
                            $args['meta_query'] = array(
                                array(
                                    'key' => $date_field_meta,
                                    'value' => array($date_after, $date_before),
                                    'meta_type' => 'DATETIME',
                                    'compare' => 'BETWEEN'
                                )
                            );
                        }
                        else if ($date_after) {
                            $args['meta_query'] = array(
                                array(
                                    'key' => $date_field_meta,
                                    'value' => $date_after,
                                    'meta_type' => 'DATETIME',
                                    'compare' => '>='
                                )
                            );
                        }
                        else {
                            $args['meta_query'] = array(
                                array(
                                    'key' => $date_field_meta,
                                    'value' => $date_before,
                                    'meta_type' => 'DATETIME',
                                    'compare' => '<='
                                )
                            );
                        }
                    }
                }
            }
            // Term query filter
            if ( in_array('term', $settings['query_filter']) ) {
                if ($settings['term_from'] == 'post_term'){
                    if($settings['include_term_'.$settings['taxonomy']] || $settings['exclude_term_'.$settings['taxonomy'] ]){
                        if($settings['include_term_'.$settings['taxonomy']] ){
                            $args['tax_query'][] = array(
                                'operator'  => $settings['include_term_operator'],
                                'taxonomy' => $settings['taxonomy'],
                                'terms'     => $settings['include_term_'.$settings['taxonomy']],
                            );
                        }
                        if($settings['exclude_term_'.$settings['taxonomy'] ]){
                            $args['tax_query'][] = array(
                                'operator'  => 'NOT IN',
                                'taxonomy' => $settings['taxonomy'],
                                'terms'     => $settings['exclude_term_'.$settings['taxonomy']],
                            );
                        }
                        if($settings['include_term_'.$settings['taxonomy']] && $settings['exclude_term_'.$settings['taxonomy']]){
                            $args['tax_query']['relation'] = 'AND';
                        }
                    }
                    if ($settings['terms_current_post']){
                        $terms_query = $this->get_terms_query($settings, $id_page);
                        if (is_array($terms_query) && !empty($terms_query)) {
                            if (count($terms_query) > 1) {
                                $taxquery['relation'] = $settings['combination_taxonomy'];
                            }
                            foreach ($terms_query as $term_query) {
                                $args['tax_query'][] = array(
                                    'taxonomy' => $settings['taxonomy'],
                                    'terms' => $term_query,
                                );
                            }
                        }
                        if(is_array($terms_query) && count($terms_query) > 1){
                            $args['tax_query']['relation'] = 'OR';
                        }
                    }
                }
                else if ($settings['term_from'] == 'post_meta'){
                    if($settings['term_field_meta']){
                        $args['tax_query'][] = array(
                            'operator'  => 'IN',
                            'taxonomy' => $settings['taxonomy'],
                            'terms'     => 'all',
                        );
                        $args['meta_query'][] = array(
                            'key'   => $settings['term_field_meta'],
                        );
                    }
                }
                else if ($settings['term_from'] == 'dynamicstring') {
                    if($settings['term_field_meta_string'] ){
                        $args['tax_query'][] = array(
                            'operator'  => 'IN',
                            'taxonomy' => $settings['taxonomy'],
                            'field'    => 'slug',
                            'terms'    => sanitize_text_field($settings['term_field_meta_string']),
                        );
                    }
                }
            }
            // Author query filter
            if ( in_array('author', $settings['query_filter']) ) {

                $author_id = get_the_author_meta('ID');

                if ( !is_singular() ) {
                    $queried_object = get_queried_object();
                    if ($queried_object) {
                        if (get_class($queried_object) == 'WP_User') {
                            $author_id = get_queried_object_id();
                            $args['author__in'] = $author_id;
                        }
                    }
                }

                if ($settings['author_from'] == 'post_author') {
                    if ($settings['include_author']) {
                        $args['author__in'] = $settings['include_author'];
                    }
                    if ($settings['exclude_author']) {
                        $args['author__not_in'] = $settings['exclude_author'];
                    }
                }
                else if ($settings['author_from'] == 'post_meta') {
                    if ($settings['author_field_meta']){
                        // sanitize_key($settings['author_field_meta_string']);

                    }
                }
                else if ($settings['author_from'] == 'dynamicstring') {
                    if ($settings['author_field_meta_string']){
                        // sanitize_text_field($settings['author_field_meta_string']);
                    }
                }
                else if ($settings['author_from'] == 'current_author') {
                    $args['author__in'] = $author_id;
                }
            }
            // Meta Key query filter
            if ( in_array('metakey', $settings['query_filter']) ) {

                if ($settings['metakey_from'] == 'post_metakey') {
                    if( $settings['include_metakey'] && $settings('include_metakey_combination') ) {
                        $args['meta_query'][]= array(
                            'key'     => $settings['include_metakey'],
                            // 'value'   => '',
                            'compare' => $settings['include_metakey_combination'],

                        );
                    }
                    if( $settings['exclude_metakey'] && $settings('exclude_metakey_combination') ) {
                        $args['meta_query'][] = array(
                            'key'     => $settings['exclude_metakey'],
                            // 'value'   => '',
                            'compare' => $settings['exclude_metakey_combination'],
                        );
                    }
                }
                else if ($settings['metakey_from'] == 'post_meta') {
                    if($settings['metakey_field_meta']){
                        $args['meta_query'][] = array(
                            'key'   => $settings['metakey_field_meta'],
                        );
                    }
                }
                else if ($settings['metakey_from'] == 'dynamicstring') {
                    if ($settings['metakey_field_meta_string']){
                        $args['meta_query'][] = array(
                            'value' => sanitize_text_field($settings['metakey_field_meta_string']),
                        );
                    }
                }
            }
        }

        $query_p = new \WP_Query( $args );
        $this->query = $query_p;
        $this->query_args = $args;
    }

}
