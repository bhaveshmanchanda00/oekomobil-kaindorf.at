<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_oeko_mobil_at' );

/** MySQL database username */
define( 'DB_USER', 'rethinkux' );

/** MySQL database password */
define( 'DB_PASSWORD', 'RethinkuxDb123!@#' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'rNTI[%@!>|lbt$t&6H1J:$[]3`}66,!>6Ypy$>Og%r-O <tGJ:DmF_}_:0H_,1Ct' );
define( 'SECURE_AUTH_KEY',  '6 Vz]2zNpp``gJ+9??OpoC_nkMCo%JrUQz4b)/9b=R820Ay%AAx[3pcYbz*&y3Vl' );
define( 'LOGGED_IN_KEY',    'tz~TLphZ_3;#J%x`&fVtHR,yO~-l_?oWS%&73q(XATSn&mZ7p6lBk!dhP*  P:WQ' );
define( 'NONCE_KEY',        '5P+R:nh:+/!Cyh,HB:MXz=xI[?ua<p,`T&SMP]6.#uvMya9#y]je_,8L5het)a:m' );
define( 'AUTH_SALT',        '1z;a.NdY1K&yxr}I^fL}n3c^5#*[Qd,P%8fPBaDRm3whTl=f<MdtQ.paQ}@+*ipc' );
define( 'SECURE_AUTH_SALT', ' M{r)~TFn&2]l`;W@Mlv96Y8)>oe$/|SUTFOmF/[U{o2ct-* X&x.y~@^yGm_0i ' );
define( 'LOGGED_IN_SALT',   'tceIs%t`*vnH_61!l@[LW(G8#%YThAUUa6-g{A630P^IhfLO+P*ma=S1<_[?;[gV' );
define( 'NONCE_SALT',       ']Uf;Wdpb0jm8@}0S%JHKWroFhHM,oytZFfE-P5(:~=>1u_A$`>Z,;dIODm.P:-Ql' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
